import { Component, OnInit, Input } from '@angular/core';
import { Enquiry } from "../../models/enquiry.model";
import { EnquiryService } from "../../services/enquiry.service";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-enquiry-list',
  templateUrl: './enquiry-list.component.html',
  styleUrls: ['./enquiry-list.component.css']
})
export class EnquiryListComponent implements OnInit {


  @Input() myEnquiries : Enquiry [];


  constructor(private router: Router, private enquiryService: EnquiryService, private route: ActivatedRoute) { }

  ngOnInit() {
  }
  onDeleteenquiry(enquiryusername:string){
    console.log(enquiryusername);
    this.enquiryService.removeEnquiry(enquiryusername);
  }

  viewenquirydetails(enquiryusername:string){
    console.log(enquiryusername);
    this.router.navigate(['/enquirydetails', enquiryusername])

  }
 

}
