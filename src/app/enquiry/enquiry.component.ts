import { Component, OnInit } from '@angular/core';
import { Enquiry } from "../models/enquiry.model";

import { EnquiryService } from "../services/enquiry.service";


@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  styleUrls: ['./enquiry.component.css'],
 // providers: [EnquiryService]
})
export class EnquiryComponent implements OnInit {

  enquiryList : Enquiry [] = [
   
   ];

  constructor(private enquiryService: EnquiryService) { }

  ngOnInit() {
/*
    this.enquiryList = this.enquiryService.getEnquiry();
   */
/*
    this.enquiryService.enquiryAdded.
    subscribe(() =>{
      this.enquiryList = this.enquiryService.getEnquiry();
    })

    */

   this.enquiryService.loadEnquiry()
   .subscribe( (result)=> { 
     this.enquiryList = this.enquiryService.getEnquiries();
   } );
      
   
       this.enquiryService.enquiryListUpdated.
       subscribe(() =>{
         this.enquiryList = this.enquiryService.getEnquiries();
       })
       
  }
/*
  onEnquiryAdded(newEnquiryInfo){

    console.log(newEnquiryInfo);
    this.enquiryList.push(newEnquiryInfo);
    console.log(this.enquiryList);
    console.log('it works');
    
  }*/

  onenquiryListUpdated(newEnquiryInfo){

    console.log(newEnquiryInfo);
    this.enquiryList.push(newEnquiryInfo);
    console.log(this.enquiryList);
    console.log('it works');
    
  }
  showadd(){
    var x = document.getElementById("enquirylis");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

}
