import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AssetComponent } from './asset/asset.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AssetDetailComponent } from './asset/asset-detail/asset-detail.component';
import { EnquiryDetailComponent } from './enquiry/enquiry-detail/enquiry-detail.component';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { GuidelineComponent } from './guideline/guideline.component';
import { LoginhomeComponent } from './loginhome/loginhome.component';
import { CampaignComponent } from './campaign/campaign.component';
import { CampaignnewComponent } from './campaign/campaignnew/campaignnew.component';
import { CampaigndetailComponent } from './campaign/campaigndetail/campaigndetail.component';

import { AuthGuardService } from './auth/auth-guard.service';


const routes: Routes = [
  {path:'', redirectTo:'/home',pathMatch:'full'},
  {path:'home',component:HomeComponent},
  {path:'asset', canActivate:[AuthGuardService], component:AssetComponent},
  {path:'contactus', component:ContactUsComponent},
  {path:'enquiry', component:EnquiryComponent},
  {path:'adminlogin', component:AdminloginComponent},
  {path:'not-found', component:PageNotFoundComponent},
  {path:'details/:productID',component:AssetDetailComponent},
  {path:'enquirydetails/:enquiryusername',component:EnquiryDetailComponent},
  {path:'guideline',component:GuidelineComponent},
  {path:'loginhome',component:LoginhomeComponent},
  {path:'campaign',component:CampaignComponent},
  {path:'campaignnew',component:CampaignnewComponent},
  {path:'campaigndetail',component:CampaigndetailComponent},

  {path:'**', redirectTo:'/home'},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
