import { Component, OnInit, Input} from '@angular/core';
import { Asset } from "../../models/asset.model";
import { AssetsService } from "../../services/assets.service";
import { CategoryService } from "../../services/category.service";
import { Router, ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/models/category.model';

@Component({
  selector: 'app-clienthome',
  templateUrl: './clienthome.component.html',
  styleUrls: ['./clienthome.component.css']
}) 
export class ClienthomeComponent implements OnInit {

  @Input() myAssets : Asset [];
  @Input() myCategory: Category[] = [];

  constructor(private router: Router, public assetsService: AssetsService, private route: ActivatedRoute, private categoryService: CategoryService) { }

  ngOnInit() { 
   
  }
 openNav() {
    document.getElementById("mySidenav").style.width = "250px";
   
    document.getElementById("container").style.marginLeft = "250px";
 
  }
  
 closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    
    document.getElementById("container").style.marginLeft= "0";
 
  }

  showEdit() {
    document.getElementById("thenew").style.display = "block";
    document.getElementById("thenewbox").style.display = "block";

 
  }

  closeEdit() {
    document.getElementById("thenew").style.display = "none";
    document.getElementById("thenewbox").style.display = "none";
   
 
  }
}
