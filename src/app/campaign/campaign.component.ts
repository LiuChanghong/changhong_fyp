import { Component, OnInit } from '@angular/core';
import { Asset } from "../models/asset.model";
import { Category } from "../models/category.model";

import { AssetsService } from "../services/assets.service";
import { CategoryService } from "../services/category.service";


@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.css']
})
export class CampaignComponent implements OnInit {

  categoryList : Category [] = [
  
  ];

 assetList : Asset [] = [
 
 ];

  constructor(private assetsService: AssetsService,private categoryService: CategoryService) { }

  ngOnInit() {

    this.assetsService.loadAsset()
    .subscribe( (result)=> { 
      this.assetList = this.assetsService.getAssets();
    } );
    
    
    this.categoryService.loadCategory()
    .subscribe( (result)=> { 
      this.categoryList = this.categoryService.getCategories();
    } );
    
    
        this.categoryList = this.categoryService.getCategories();
    
        this.assetsService.assetListUpdated.
        subscribe(() =>{
          this.assetList = this.assetsService.getAssets();
        })
  }

  onassetListUpdated(newAssetInfo){

    console.log(newAssetInfo);
    this.assetList.push(newAssetInfo);
    console.log(this.assetList);
    console.log('it works');
    
  }

  
} 
