import { Component, OnInit, Input } from '@angular/core';
import { Asset } from "../../../models/asset.model";
import { AssetsService } from "../../../services/assets.service";
import { CategoryService } from "../../../services/category.service";
import { Router, ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/models/category.model';
import { Location } from '@angular/common';

@Component({
  selector: 'app-campaignlist',
  templateUrl: './campaignlist.component.html',
  styleUrls: ['./campaignlist.component.css']
})
export class CampaignlistComponent implements OnInit {

  @Input() myAssets : Asset [];
  @Input() myCategory: Category[] = [];
  @Input() displayAssets: Asset [];
  @Input() assetList: Asset[] =[];
  @Input() categoryList: Category[] = [];
  @Input() selectedCategory: number;

  constructor(private router: Router, public assetsService: AssetsService, private route: ActivatedRoute, private categoryService: CategoryService, public location: Location) { }

  ngOnInit() {
    this.assetsService.assetListUpdated.subscribe(() => {
      this.assetList = this.assetsService.getAssets();
    });

    this.assetsService.loadAsset().subscribe(()=> {
      this.assetList = this.assetsService.getAssets();
      this.displayAssets = this.assetsService.getAssets();
    });

    this.categoryService.loadCategory().subscribe(()=> {
      this.categoryList = this.categoryService.getCategories();
    });

    this.categoryService.categoryListUpdated.subscribe(() => {
      this.categoryList = this.categoryService.getCategories();
    });

  }
  
  viewdetails(assetID:string){
    console.log(assetID);
    this.router.navigate(['/details', assetID])

  }

  onDelete(assetID:string){
    console.log(assetID);
    this.assetsService.removeAsset(assetID);
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
   
    document.getElementById("container").style.marginLeft = "250px";
 
  }
  
 closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    
    document.getElementById("container").style.marginLeft= "0";
 
  }

  showDetail(){
    document.getElementById("assetshade").style.display= "block";
    document.getElementById("assetdetail").style.display= "block";
  }

  closeDetail(){
    document.getElementById("assetshade").style.display= "none";
    document.getElementById("assetdetail").style.display= "none";
  }

  onChange(category): void {
    const val = category.target.value;
    console.log("value " + val);
    
    if (val === 'All') {
      this.location.replaceState(`/assets/`);
      this.displayAssets = this.assetList;
    } else {
      this.location.replaceState(`/assets/${val}`);
      this.displayAssets = this.assetList.filter(asset => asset.categoryID === val);
  
      console.log(this.displayAssets);
    } 
  }


  
  showUpload() {
    document.getElementById("thenew").style.display = "block";
    document.getElementById("thenewbox").style.display = "block";

 
  }

  closeUpload() {
    document.getElementById("thenew").style.display = "none";
    document.getElementById("thenewbox").style.display = "none";
   
 
  }

  showUsage(){
    document.getElementById("detailword").style.display = "none";
    document.getElementById("detailinsight").style.display = "Block";
    document.getElementById("imgusage").style.backgroundColor = "black";
    document.getElementById("imgusage").style.color = "white";

    document.getElementById("imgoverview").style.backgroundColor = "white";
    document.getElementById("imgoverview").style.color = "black";
  }
  showOverview(){
    document.getElementById("detailword").style.display = "block";
    document.getElementById("detailinsight").style.display = "none";
    document.getElementById("imgoverview").style.backgroundColor = "black";
    document.getElementById("imgoverview").style.color = "white";

    document.getElementById("imgusage").style.backgroundColor = "white";
    document.getElementById("imgusage").style.color = "black";
  }

}
