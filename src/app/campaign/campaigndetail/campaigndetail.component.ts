import { Component, OnInit } from '@angular/core';
import { Asset } from "../../models/asset.model";
import { Category } from "../../models/category.model";

import { AssetsService } from "../../services/assets.service";
import { CategoryService } from "../../services/category.service";

@Component({
  selector: 'app-campaigndetail',
  templateUrl: './campaigndetail.component.html',
  styleUrls: ['./campaigndetail.component.css']
})
export class CampaigndetailComponent implements OnInit {

  categoryList : Category [] = [
    /* new Category('1','laptop','portable computer')*/
   ];

  assetList : Asset [] = [
   /* new Asset('1','Laptop','this is a normal laptop','Alienware','$20','1','image11.png')*/
  ];
  
  constructor(private assetsService: AssetsService,private categoryService: CategoryService) { }


  ngOnInit() {
    
    /*
    this.assetList = this.assetsService.getAssets();

*/
this.assetsService.loadAsset()
.subscribe( (result)=> { 
  this.assetList = this.assetsService.getAssets();
} );


this.categoryService.loadCategory()
.subscribe( (result)=> { 
  this.categoryList = this.categoryService.getCategories();
} );


    this.categoryList = this.categoryService.getCategories();

    this.assetsService.assetListUpdated.
    subscribe(() =>{
      this.assetList = this.assetsService.getAssets();
    })
    
  }

  onassetListUpdated(newAssetInfo){

    console.log(newAssetInfo);
    this.assetList.push(newAssetInfo);
    console.log(this.assetList);
    console.log('it works');
    
  }

  
    showadd(){
    var x = document.getElementById("addasset");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }
 
}
