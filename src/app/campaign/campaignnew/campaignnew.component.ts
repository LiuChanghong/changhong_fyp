import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-campaignnew',
  templateUrl: './campaignnew.component.html',
  styleUrls: ['./campaignnew.component.css']
})
export class CampaignnewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
   
    document.getElementById("container").style.marginLeft = "250px";
 
  }
  
 closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    
    document.getElementById("container").style.marginLeft= "0";
 
  }

 
  showUpload() {
    document.getElementById("thenew").style.display = "block";
    document.getElementById("thenewbox").style.display = "block";

 
  }

  closeUpload() {
    document.getElementById("thenew").style.display = "none";
    document.getElementById("thenewbox").style.display = "none";
   
 
  }
}
