import { Component, OnInit, Input } from '@angular/core';
import { Asset } from "../../models/asset.model";
import { AssetsService } from "../../services/assets.service";
import { CategoryService } from "../../services/category.service";
import { Router, ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/models/category.model';
import { Location } from '@angular/common';

@Component({
  selector: 'app-campaigncontent',
  templateUrl: './campaigncontent.component.html',
  styleUrls: ['./campaigncontent.component.css']
})
export class CampaigncontentComponent implements OnInit {

  @Input() myAssets : Asset [];
  @Input() myCategory: Category[] = [];
  @Input() displayAssets: Asset [];
  @Input() assetList: Asset[] =[];
  @Input() categoryList: Category[] = [];
  @Input() selectedCategory: number;

  constructor(private router: Router, public assetsService: AssetsService, private route: ActivatedRoute, private categoryService: CategoryService, public location: Location) { }

  ngOnInit() {
    this.assetsService.assetListUpdated.subscribe(() => {
      this.assetList = this.assetsService.getAssets();
    });

    this.assetsService.loadAsset().subscribe(()=> {
      this.assetList = this.assetsService.getAssets();
      this.displayAssets = this.assetsService.getAssets();
    });

    this.categoryService.loadCategory().subscribe(()=> {
      this.categoryList = this.categoryService.getCategories();
    });

    this.categoryService.categoryListUpdated.subscribe(() => {
      this.categoryList = this.categoryService.getCategories();
    });
  }
  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
   
    document.getElementById("container").style.marginLeft = "250px";
 
  }
  
 closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    
    document.getElementById("container").style.marginLeft= "0";
 
  }

  showNew() {
    document.getElementById("thenew").style.display = "block";
    document.getElementById("thenewbox").style.display = "block";
   
 
  }

  closeNew() {
    document.getElementById("thenew").style.display = "none";
    document.getElementById("thenewbox").style.display = "none";
   
 
  }

  onChange(category): void {
    const val = category.target.value;
    console.log("value " + val);
    
    if (val === 'All') {
      this.location.replaceState(`/assets/`);
      this.displayAssets = this.assetList;
    } else {
      this.location.replaceState(`/assets/${val}`);
      this.displayAssets = this.assetList.filter(asset => asset.categoryID === val);
  
      console.log(this.displayAssets);
    } 
  }

}
