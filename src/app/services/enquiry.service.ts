import { EventEmitter, Injectable } from '@angular/core';
import { Enquiry } from '../models/enquiry.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from "rxjs/operators";

import { environment } from "../../environments/environment";

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class EnquiryService {

  enquiryAdded = new EventEmitter<void>();
  enquiryListUpdated = new EventEmitter<void>();
  private enquiryList : Enquiry [] = [


  ];

  constructor(public httpClient: HttpClient) { }

  loadEnquiry(){
    return this.httpClient.get<Enquiry[]>("http://localhost:3000/api/Enquiry"/*${APIEndpoint}/api/Asset */)
    .pipe(map( (Enquiry)=>{
  this.enquiryList = Enquiry;
  return Enquiry;
    } , (error)=>{
  
  console.log("error")
    } ));
    }

    
  addEnquiry(newEnquiryInfo){

 /*   this.enquiryList.push(newEnquiryInfo);
    this.enquiryAdded.emit();*/

    const httpHeaders = new HttpHeaders({
      'Content-type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    }

    this.httpClient.post <Enquiry> 
      ("http://localhost:3000/api/Enquiry", {info: newEnquiryInfo}, options)
      .subscribe((respond)=> {
        this.enquiryList.push(respond);
        this.enquiryListUpdated.emit();
      });

  }



  removeEnquiry(enquiryusername){
    this.httpClient.delete <{ success : boolean }>(`${APIEndpoint}/api/Enquiry/${enquiryusername}`/*`${APIEndpoint}/api/Asset/${productID}`*/)
    .subscribe((respond)=>{
      if (respond.success){
          this.loadEnquiry().subscribe(()=>{
            this.enquiryListUpdated.emit();
          });
      }
    });
  }

  

  getEnquiries(){
    return this.enquiryList.slice();
  }


  getEnquiry(enquiryusername: string) {

    console.log(this.enquiryList + "is the list");
    for (let enquiry of this.enquiryList) {
      if (enquiry.enquiryusername == enquiryusername) {
        console.log(enquiry);
        return enquiry;

      }
    }

    return undefined;
  }

  getChosenEnquiry(enquiryusername: string) { 
    return this.enquiryList.find( 
      Enquiry => { return Enquiry.enquiryusername == enquiryusername }, enquiryusername 
    );
  }
  updateEnquiry(updateInfo:Enquiry) {
    const enquiryusername = updateInfo.enquiryusername;
    delete updateInfo['enquiryusername'];
    return this.httpClient.put <{success: boolean}>
      (`${APIEndpoint}/api/enquiry/${enquiryusername}`, {info: updateInfo})
        .pipe(map(
          (result) => {
            return (result.success == true);
          },
          (error) => {
            console.log(error);
            return false;
          }
        ));
  }

}
