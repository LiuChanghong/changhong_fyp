import { EventEmitter, Injectable } from '@angular/core';
import { Asset } from "../models/asset.model";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from "rxjs/operators";

import { environment } from "../../environments/environment";

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class AssetsService {

  assetListUpdated = new EventEmitter<void>();

  private assetList: Asset[] = [/*
    new Asset('1','Zephyrus M','Thin and Portable Gaming Laptop, 15.6” 240Hz FHD IPS, NVIDIA GeForce RTX 2070, Intel Core i7-9750H, 16GB DDR4 RAM, 1TB PCIe SSD, Per-Key RGB, Windows 10 Home, GU502GW-AH76','ROG','$1,849.99','1','/assets/img/ZephyrusM.jpg'),
    new Asset('2','ZenBook Pro','Duo UX581 15.6” 4K UHD NanoEdge Bezel Touch, Intel Core i7-9750H, 16GB RAM, 1TB PCIe SSD, GeForce RTX 2060, Innovative Screenpad Plus, Windows 10 Pro - UX581GV-XB74T, Celestial Blue','ASUS','$2,378.21','1','/assets/img/Zenbook.jpg'),
    new Asset('3','VivoBook S','15.6in Full HD Laptop, Intel Core i7-8550U, NVIDIA GeForce MX150, 8GB RAM, 256GB SSD + 1TB HDD, Windows 10 (Renewed)','ASUS','$749.66','1','/assets/img/vivobooks.jpg'),
    new Asset('4','M17','8th Gen Intel Core i7-8750H 6-Core | 17.3 Inch FHD 1920x1080 60Hz IPS | 16GB 2666MHz DDR4 RAM | 512GB SSD| NVIDIA GeForce RTX 2070 Max Q','Alienware','$1,499.95','1','/assets/img/M17.jpg'),
    new Asset('5','ThinkPad P71','Windows 10 Pro - Xeon E3-1535M, 64GB ECC RAM, 4TB SSD, 17.3" UHD 4K 3840x2160 Display, Quadro P4000 8GB GPU, Color Sensor, , 4G LTE WWAN','Lenovo','$6,049.12','1','/assets/img/ThinkPad.jpg'),
    new Asset('6','IPhone 11 Pro','6. 5-inch Super Retina XDR OLED display, Water and dust resistant, Triple-camera system with 12MP Ultra wide, wide, and telephoto cameras; night mode, Portrait mode','Apple','$1,099.00','2','/assets/img/Iphone11promax.jpg'),
    new Asset('7','Redmi Note 8','64GB + 4GB RAM, 6.3" LTE 48MP Factory Unlocked GSM Smartphone - International Version (Moonlight White) 2MP - Front Camera: 13 MP, f/2.0 - Video: 2160p@30fps, 1080p@30/60/120fps, 720p@960fps ','Xiaomi','$174.50','2','/assets/img/redmi.jpg'),
    new Asset('8','MI Mix 3','6.39" Display, Dual SIM 4G LTE GSM Unlocked Multi-Functional Magnetic Slider Smartphone w/Wireless Charging Pad (Black) AI music pairing for instant audiovisual beauty, 24MP + 2MP-front camera','Xiaomi','$519.00','2','/assets/img/mimix.jpg'),
    new Asset('9','Mate 10 Pro','6" 6GB/128GB, AI Processor, Dual Leica Camera, Water Resistant IP67, GSM Only - Titanium Gray With a large 4000 mAh battery coupled with smart battery management that learns from user behaviors','Huawei','$479.99','2','/assets/img/mate10.jpg'),
    new Asset('10','Galaxy S10','An immersive Cinematic Infinity Display, Pro grade Camera and Wireless PowerShare The next generation is here, Intelligently accesses power by learning how and when you use your phone.','Samsung','$849.99','2','/assets/img/s10.jpg'),
    new Asset('11','AirPods Pro','Active noise cancellation for immersive sound, Transparency mode for hearing and connecting with the world around you, Three sizes of soft, tapered silicone tips for a customizable fit','Apple','$199.99','3','/assets/img/airpodspro.jpg'),
    new Asset('12','Mi AirDots','Wireless Headphones Bluetooth V5.0 True Wireless Stereo Wireless Earphones with Wirelss Charging Case 12Hours Battery Life (Redmi Airdots), Bluetooth v5.0','Xiaomi','$30.98','3','/assets/img/airdots.jpg'),
    new Asset('13','Powerbeats 3','Connectivity Technology: Wireless Connect via Class 1 Bluetooth with your device for wireless workout freedom, Up to 12 hours of battery life to power through multiple workouts','Beats','$79.99','3','/assets/img/powerbeats.jpg'),
    new Asset('14','Galaxy Buds','Bluetooth True Wireless Earbuds (Wireless Charging Case Included), Black - International Version, No Warranty， Premium sound Tuned by AKG','Samsung','$149.99','3','/assets/img/galaxybuds.jpg'),
    new Asset('15','AirPods','Automatically on, automatically connected, asy setup for all your Apple devices, Double-tap to play or skip forward, Charges quickly in the case','Apple','$125.99','3','/assets/img/airpods.jpg'),
*/
  ];

  constructor(public httpClient: HttpClient) { }


  loadAsset() {
    return this.httpClient.get<Asset[]>("http://localhost:3000/api/Asset"/*${APIEndpoint}/api/Asset */)
      .pipe(map((Asset) => {
        this.assetList = Asset;
        console.log(this.assetList);
        console.log("theting");
        return Asset;
       
      }, (error) => {

        console.log("error")
      }));
  }



  addAsset(newAssetInfo) {
    const httpHeaders = new HttpHeaders({
      'Content-type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    }

    this.httpClient.post<Asset>
      (/*`${APIEndpoint}/api/Assets`*/"http://localhost:3000/api/Asset", { info: newAssetInfo }, options)
      .subscribe((respond) => {
        this.assetList.push(respond);
        this.assetListUpdated.emit();
      });
    /* this.assetList.push(newAssetInfo);
     this.assetListUpdated.emit();*/

  }

  getAssets() {
    return this.assetList.slice();
  }

  getAsset(productID: number) {

    console.log(this.assetList + "is the list");
    for (let asset of this.assetList) {
      if (asset.productID == productID) {
        return asset;
        console.log(asset);

      }
    }

    return undefined;
  }

  // getChosenAssets(productID: string) {
  //   return this.assetList.find(
  //     Asset => { return Asset.productID == productID }, productID
  //   );
  // }

  removeAsset(productID) {
    this.httpClient.delete<{ success: boolean }>(`${APIEndpoint}/api/Asset/${productID}`/*`${APIEndpoint}/api/Asset/${productID}`*/)
      .subscribe((respond) => {
        if (respond.success) {
          this.loadAsset().subscribe(() => {
            this.assetListUpdated.emit();
          });
        }
      });
  }

  updateAsset(updateInfo: Asset) {
    const productID = updateInfo.productID;
    delete updateInfo['productID'];
    return this.httpClient.put <{success: boolean}>
      (`${APIEndpoint}/api/asset/${productID}`, {info: updateInfo})
        .pipe(map(
          (result) => {
            return (result.success == true);
          },
          (error) => {
            console.log(error);
            return false;
          }
        ));
  }

}
