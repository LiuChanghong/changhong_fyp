import {
  Directive,
  ElementRef,
  Renderer2,
  HostBinding,
  Input,
  HostListener
} from '@angular/core';


@Directive({
  selector: '[appHoverHighlight]'
})
export class HoverHighlightDirective {

  @Input('appHoverHighlight') highlightColor: {background:string, leave:string};

  @HostBinding('style.color') textColor: string;
  @HostBinding('style.fontWeight') weight: string;

  constructor(private elRef: ElementRef, private renderer: Renderer2) { }

  @HostListener ('mouseenter') mouseOver(eventData: Event) {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', this.highlightColor.background);

  }
  @HostListener ('mouseleave') mouseExit(eventData: Event) {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', this.highlightColor.leave);
   
  }

}