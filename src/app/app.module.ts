import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { ReactiveFormsModule } from "@angular/forms"
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AssetComponent } from './asset/asset.component';
import { AssetAddComponent } from './asset/asset-add/asset-add.component';
import { AssetListComponent } from './asset/asset-list/asset-list.component';
import { HomeComponent } from './home/home.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HeaderComponent } from './header/header.component';
import { AssetDetailComponent } from './asset/asset-detail/asset-detail.component';
import { SearchPipe } from './search.pipe';
import { CategoryPipe } from './category.pipe';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { EnquiryListComponent } from './enquiry/enquiry-list/enquiry-list.component';
import { EnquiryAddComponent } from './enquiry/enquiry-add/enquiry-add.component';
import { AuthService } from './auth/auth.service'
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { HoverHighlightDirective } from './hover-highlight.directive';
import { HoverHighlightgreenDirective } from './hover-highlightgreen.directive';
import { HoverHighlightwhiteDirective } from './hover-highlightwhite.directive';
import { EnquiryDetailComponent } from './enquiry/enquiry-detail/enquiry-detail.component';
import { GuidelineComponent } from './guideline/guideline.component';
import { LoginhomeComponent } from './loginhome/loginhome.component';
import { ClienthomeComponent } from './loginhome/clienthome/clienthome.component';
import { CampaignComponent } from './campaign/campaign.component';
import { CampaigncontentComponent } from './campaign/campaigncontent/campaigncontent.component';
import { CampaignnewComponent } from './campaign/campaignnew/campaignnew.component';
import { CampaigndetailComponent } from './campaign/campaigndetail/campaigndetail.component';
import { CampaignlistComponent } from './campaign/campaigndetail/campaignlist/campaignlist.component';

@NgModule({
  declarations: [
    AppComponent,
    AssetComponent,
    AssetAddComponent,
    AssetListComponent,
    HomeComponent,
    ContactUsComponent,
    PageNotFoundComponent,
    HeaderComponent,
    AssetDetailComponent,
    SearchPipe,
    CategoryPipe,
    EnquiryComponent,
    EnquiryListComponent,
    EnquiryAddComponent,
    AdminloginComponent,
    HoverHighlightDirective,
    HoverHighlightgreenDirective,
    HoverHighlightwhiteDirective,
    EnquiryDetailComponent,
    GuidelineComponent,
    LoginhomeComponent,
    ClienthomeComponent,
    CampaignComponent,
    CampaigncontentComponent,
    CampaignnewComponent,
    CampaigndetailComponent,
    CampaignlistComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
