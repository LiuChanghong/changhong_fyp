import { Component, OnInit,Input } from '@angular/core';
import { Asset } from '../../models/asset.model'
import { Category } from '../../models/category.model'
import { Router, ActivatedRoute } from '@angular/router';
import { AssetsService } from "../../services/assets.service";
import { CategoryService } from "../../services/category.service";

@Component({
  selector: 'app-asset-detail',
  templateUrl: './asset-detail.component.html',
  styleUrls: ['./asset-detail.component.css']
})
export class AssetDetailComponent implements OnInit {

  showEditForm: boolean = false; 

  @Input() myCategory: Category[] = [];

  // chosenproduct: Asset = new Asset(0,"","","","","","");
  // chosencategory: Category = new Category("","","")
  categoryList: Category[] = [];
  assetList: Asset [] = [];
  selectedAsset: Asset = new Asset(0,"", "", "", "", "", "");

  constructor(public assetsService: AssetsService,public categoryService: CategoryService, public router: Router, public route: ActivatedRoute) { }

  

  ngOnInit() {
    /*
    this.chosenproduct = this.assetsService.getChosenAssets(this.Route.snapshot.params['productID'] );
    console.log(this.chosenproduct);

*/
const productID = this.route.snapshot.params['productID'];

    console.log(`The productID is ${productID}`);

    const searchResult = this.assetsService.getAsset(productID);

    console.log(searchResult+"is searchresult");

    if (searchResult != undefined) {
      this.selectedAsset = searchResult;
    } else {
      this.router.navigate(['/adminlogin'])
      alert("something wrong");
    }

  }
  onUpdate(name: string, description: string, brand: string, price:string, categoryID:string,imageURL:string ) {
    let updateInfo = new Asset(this.selectedAsset.productID, name, description, brand,price,categoryID,imageURL);
    this.assetsService.updateAsset(updateInfo).subscribe(
      (success) => {
        this.toggleShowEditForm();
        if (success) {
          alert("Asset information Updated");
          this.selectedAsset.name    = updateInfo.name; 
          this.selectedAsset.description   = updateInfo.description; 
          this.selectedAsset.brand = updateInfo.brand; 
          this.selectedAsset.price = updateInfo.price; 
          this.selectedAsset.categoryID = updateInfo.categoryID; 
          this.selectedAsset.imageURL = updateInfo.imageURL; 
        } else {
          alert("Update Failed, Please try again.");
        }
      }
    );
  }

  toggleShowEditForm() {
    this.showEditForm = !this.showEditForm;
  }
}
