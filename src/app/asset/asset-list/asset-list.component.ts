import { Component, OnInit, Input } from '@angular/core';
import { Asset } from "../../models/asset.model";
import { AssetsService } from "../../services/assets.service";
import { CategoryService } from "../../services/category.service";
import { Router, ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/models/category.model';
import { Location } from '@angular/common';

@Component({
  selector: 'app-asset-list',
  templateUrl: './asset-list.component.html',
  styleUrls: ['./asset-list.component.css']
})
export class AssetListComponent implements OnInit {

  @Input() myAssets : Asset [];
  @Input() myCategory: Category[] = [];
   displayAssets: Asset [];
   assetList: Asset[] =[];
   categoryList: Category[] = [];
   selectedCategory: number;
   selectedAsset: Asset = new Asset (0 ,"" ,"", "", "", "", "");


  constructor(public router: Router, public assetsService: AssetsService, public route: ActivatedRoute, public categoryService: CategoryService, public location: Location) { }

  ngOnInit() {

    this.assetsService.assetListUpdated.subscribe(() => {
      this.assetList = this.assetsService.getAssets();
    });

    this.assetsService.loadAsset().subscribe(()=> {
      this.assetList = this.assetsService.getAssets();
      this.displayAssets = this.assetsService.getAssets();
    });

    this.categoryService.loadCategory().subscribe(()=> {
      this.categoryList = this.categoryService.getCategories();
    });

    this.categoryService.categoryListUpdated.subscribe(() => {
      this.categoryList = this.categoryService.getCategories();
    });

  
  }
  
  // viewdetails(productID: number){
  //   console.log(productID);
  //   this.router.navigate(['/details/, productID'])

  // }

  onDelete(productID:string){
   
    this.assetsService.removeAsset(productID);
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
   
    document.getElementById("container").style.marginLeft = "250px";
 
  }
  
 closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    
    document.getElementById("container").style.marginLeft= "0";
 
  }

  showDetail(productID: number, description: string){
    document.getElementById("assetshade").style.display= "block";
    document.getElementById("assetdetail").style.display= "block";
    document.getElementById("assetid").innerHTML = "-" + productID;
    document.getElementById("assetdesc").innerHTML = "-" + description;
    console.log("assetdetail_"+ productID);
  
    
    // this.router.navigate(["/details/", productID]);
  }

  closeDetail(){
    document.getElementById("assetshade").style.display= "none";
    document.getElementById("assetdetail").style.display= "none";
  }
  showUpload() {
    document.getElementById("thenew").style.display = "block";
    document.getElementById("thenewbox").style.display = "block";

 
  }

  closeUpload() {
    document.getElementById("thenew").style.display = "none";
    document.getElementById("thenewbox").style.display = "none";
   
 
  }
  showFilter() {
    document.getElementById("filterpart").style.display = "block";
    document.getElementById("mySidenav").style.width = "0";
    
    document.getElementById("container").style.marginLeft= "0";

 
  }

  closeFilter() {
    document.getElementById("filterpart").style.display = "none";
   
   
 
  }
  showUsage(){
    document.getElementById("detailword").style.display = "none";
    document.getElementById("detailinsight").style.display = "Block";
    document.getElementById("imgusage").style.backgroundColor = "black";
    document.getElementById("imgusage").style.color = "white";

    document.getElementById("imgoverview").style.backgroundColor = "white";
    document.getElementById("imgoverview").style.color = "black";
  }
  showOverview(){
    document.getElementById("detailword").style.display = "block";
    document.getElementById("detailinsight").style.display = "none";
    document.getElementById("imgoverview").style.backgroundColor = "black";
    document.getElementById("imgoverview").style.color = "white";

    document.getElementById("imgusage").style.backgroundColor = "white";
    document.getElementById("imgusage").style.color = "black";
  }
  onChange(category): void {
    const val = category.target.value;
    console.log("value " + val);
    
    if (val === 'All') {
      this.location.replaceState(`/assets/`);
      this.displayAssets = this.assetList;
    } else {
      this.location.replaceState(`/assets/${val}`);
      this.displayAssets = this.assetList.filter(asset => asset.categoryID === val);
  
      console.log(this.displayAssets);
    } 
  }
}