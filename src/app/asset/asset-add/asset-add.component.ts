import { Component, OnInit /*, ElementRef, ViewChild, EventEmitter, Output*/} from '@angular/core';
import { Asset } from "../../models/asset.model";
import { AssetsService } from "../../services/assets.service";
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-asset-add',
  templateUrl: './asset-add.component.html',
  styleUrls: ['./asset-add.component.css']
})
export class AssetAddComponent implements OnInit {

  emailBlackList = ['test@test.com', 'temp@temp.com'];

  submitted = false;

  AssetForm: FormGroup;

  inputInfo : Asset = new Asset(0,"","","","","","")
/*
  @ViewChild('inputProductID',{static:false}) inputProductID: ElementRef;
  @ViewChild('inputName',{static:false}) inputName: ElementRef;
  @ViewChild('inputDescription',{static:false}) inputDescription: ElementRef;
  @ViewChild('inputBrand',{static:false}) inputBrand: ElementRef;
  @ViewChild('inputPrice',{static:false}) inputPrice: ElementRef;
  @ViewChild('inputCategoryID',{static:false}) inputCategoryID: ElementRef;
  @ViewChild('inputImageURL',{static:false}) inputImageURL: ElementRef;

  @Output() assetListUpdated = new EventEmitter<Asset>();
*/
  constructor(private assetsService : AssetsService) { }

  ngOnInit() {
    this.AssetForm = new FormGroup({
     
      'inputName'  : new FormControl(null, [Validators.required, this.blankSpaces]),
      'inputDescription': new FormControl(null, [Validators.required, this.blankSpaces]),
      'inputBrand': new FormControl(null, [Validators.required, this.blankSpaces]),
      'inputPrice': new FormControl(null, [Validators.required, this.blankSpaces]),
     /* 'inputCategoryID': new FormControl(null, [Validators.required, this.blankSpaces]),*/
      'inputImageURL': new FormControl(null, [Validators.required, this.blankSpaces])
      
    });
  }

  blankSpaces(control: FormControl): {[s: string]: boolean}  {
    if (control.value != null && control.value.trim().length === 0) {
       return {'blankSpaces': true};
    }
    return null;
  }

  inEmailBlackList(control: FormControl): {[s: string]: boolean} {
    if (this.emailBlackList.indexOf(control.value) !== -1) {
      return {'emailBlackListed': true};
    }
    return null;
  }

  onSubmit() {
    console.log(this.AssetForm);
    this.submitted = true;
  }

  
  onAddAsset(inputProductID,
    inputName:HTMLInputElement,
    inputDescription:HTMLInputElement,
    inputBrand:HTMLInputElement,
    inputPrice:HTMLInputElement,
    inputCategoryID:HTMLInputElement,
    inputImageURL:HTMLInputElement){
 /*
    console.log("add asset");
    console.log(this.inputInfo);
    console.log(this.inputProductID.nativeElement.value);
    console.log(this.inputName.nativeElement.value);
    console.log(this.inputDescription.nativeElement.value);
    console.log(this.inputBrand.nativeElement.value);
    console.log(this.inputPrice.nativeElement.value);
    console.log(this.inputCategoryID.nativeElement.value);
    console.log(this.inputImageURL.nativeElement.value);
  */
 /*console.log(inputDescription.value);
  this.assetListUpdated.emit(new Asset(
      inputProductID.value,
      inputName.value,
      inputDescription.value,
      inputBrand.value,
      inputPrice.value,
      inputCategoryID.value,
      inputImageURL.value


    ));  
    */
   if (inputName.value == ""||inputDescription.value == ""||inputBrand.value == ""||inputPrice.value == ""||inputCategoryID.value == ""||inputImageURL.value == "") {
    alert ("Wrong or empty Information, please try again!")
    return false;
  } else
    this.assetsService.addAsset(new Asset(
      inputProductID.value,
      inputName.value,
      inputDescription.value,
      inputBrand.value,
      inputPrice.value,
      inputCategoryID.value,
      inputImageURL.value
    ));
    alert("New Asset Added")
  }

}
