

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-guideline',
  templateUrl: './guideline.component.html',
  styleUrls: ['./guideline.component.css']
})
export class GuidelineComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
   
    document.getElementById("container").style.marginLeft = "250px";
 
  }
  
 closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    
    document.getElementById("container").style.marginLeft= "0";
 
  }

}