import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService{

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
  Observable<boolean> |Promise<boolean> | Boolean{
  
    return this.authService.isAuthenticated()
    .then((authenticated: boolean) => {
      if (authenticated){
        return true;
      } else {
        console.log(" access denied...");
        this.router.navigate(['/adminlogin']);
      }
    });
  }


}
