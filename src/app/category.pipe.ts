import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'category'
})
export class CategoryPipe implements PipeTransform {

  transform(assets: any, propertyName: string, searchStr2: string): any {

    let selectedcate = [];

    if (assets.length === 0 || searchStr2 == null) { 
      return assets;
    }

   

    selectedcate = assets.filter(asset => { 
      return asset.categoryID.indexOf(searchStr2) >= 0 
    });

    return selectedcate;
  }

}
