import { Directive } from '@angular/core';
import {
 
  ElementRef,
  Renderer2,
  HostBinding,
  Input,
  HostListener
} from '@angular/core';
@Directive({
  selector: '[appHoverHighlightgreen]'
})
export class HoverHighlightgreenDirective {


  @Input('appHoverHighlight') highlightColor: {background:string, text:string};

  @HostBinding('style.color') textColor: string;
  @HostBinding('style.fontWeight') weight: string;

  constructor(private elRef: ElementRef, private renderer: Renderer2) { }

  @HostListener ('mouseenter') mouseOver(eventData: Event) {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', '#000d00');
    
  }
  @HostListener ('mouseleave') mouseExit(eventData: Event) {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'black');
  
  }

}
