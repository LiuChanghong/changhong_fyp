export class Asset {

    public productID: number;
    public name: string;
    public description: string;
    public brand: string;
    public price: string;
    public categoryID: string;
    public imageURL: string;

    constructor(productID: number, name:string, description:string, brand:string, price: string, categoryID: string, imageURL: string){
         this.productID = productID;
         this.name = name;
         this.description = description;
         this.brand = brand;
         this.price = price;
         this.categoryID = categoryID;
         this.imageURL = imageURL;

    }


}